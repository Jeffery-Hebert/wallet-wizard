# Routers Assets.py


from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from models.assets_models import AssetIn, AssetOut, AssetsList
from queries.asset_queries import Assets
from queries.net_worth_queries import NetWorth
from utils import VerifyToken

router = APIRouter()
token_auth_scheme = HTTPBearer()


@router.post("/api/assets/new", response_model=AssetOut)
def create_asset(
    new_asset: AssetIn,
    asset_queries: Assets = Depends(),
    net_worth_queries: NetWorth = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = asset_queries.create(new_asset, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result


@router.get("/api/assets", response_model=AssetsList)
def get_all_asset(
    asset_queries: Assets = Depends(),
    token: str = Depends(token_auth_scheme),
):

    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = asset_queries.getlist(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/asset/{id}", response_model=AssetOut)
def get_asset_by_id(
    id: str,
    asset_queries: Assets = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = asset_queries.getindv(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.delete("/api/asset/delete/{id}")
def delete_asset_by_id(
    id: str,
    asset_queries: Assets = Depends(),
    net_worth_queries: NetWorth = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = asset_queries.delete(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result


@router.put("/api/asset/{id}")
def update_asset_by_id(
    id: str,
    asset: AssetIn,
    asset_queries: Assets = Depends(),
    token: str = Depends(token_auth_scheme),
    net_worth_queries: NetWorth = Depends(),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = asset_queries.edit(id, asset, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result
