import os
import openai
from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from utils import VerifyToken
from models.advisor_models import QueryIn

router = APIRouter()
token_auth_scheme = HTTPBearer()

openai.api_key = os.environ.get("API_KEY")


@router.post("/api/advisor/new")
def single_response(
    query: QueryIn,
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {
                "role": "system",
                "content": "You are my private wealth advisor.",
            },
            {"role": "user", "content": query.query},
        ],
        temperature=0.4,
        max_tokens=1024,
    )
    result = response["choices"][0]["message"]["content"]
    return result
