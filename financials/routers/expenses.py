# Routers expenses.py


from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from models.expenses import (
    ExpenseIn,
    ExpenseOut,
    ExpensesList,
    ExpensesTypeList,
)
from queries.expense_queries import Expenses
from utils import VerifyToken

router = APIRouter()
token_auth_scheme = HTTPBearer()


@router.post("/api/expenses/new", response_model=ExpenseOut)
def create_expense(
    new_expense: ExpenseIn,
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.create(new_expense, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/expenses", response_model=ExpensesList)
def get_all_expense(
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.getlist(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/expense/{id}", response_model=ExpenseOut)
def get_expense_by_id(
    id: str,
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.getindv(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.delete("/api/expense/delete/{id}")
def delete_expense_by_id(
    id: str,
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.delete(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.put("/api/expense/{id}")
def update_expense_by_id(
    id: str,
    expense: ExpenseIn,
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.edit(id, expense, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/expenses/categories", response_model=ExpensesTypeList)
def get_expense_type_totals(
    expense_queries: Expenses = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = expense_queries.totalByCategory(userID)
    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result
