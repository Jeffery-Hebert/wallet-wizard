fastapi[all]>=0.81.0
psycopg[binary,pool]>=3.0.14
pydantic>=1.10.4
pyjwt[crypto]
pytest
uvicorn[standard]>=0.17.6
openai
