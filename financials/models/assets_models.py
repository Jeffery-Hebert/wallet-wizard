from pydantic import BaseModel


class AssetIn(BaseModel):
    name: str
    value: int
    type: str


class AssetOut(BaseModel):
    id: str
    name: str
    value: int
    type: str


class AssetsList(BaseModel):
    assets: list[AssetOut]
