from pydantic import BaseModel


class ExpenseIn(BaseModel):
    name: str
    amount: int
    type: str


class ExpenseOut(BaseModel):
    id: str
    name: str
    amount: int
    type: str


class ExpenseTypeOut(BaseModel):
    type: str
    total: int


class ExpensesList(BaseModel):
    expenses: list[ExpenseOut]


class ExpensesTypeList(BaseModel):
    types: list[ExpenseTypeOut]
