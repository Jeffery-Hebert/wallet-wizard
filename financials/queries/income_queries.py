from models.income import IncomeOut, IncomeList
from queries.pool import pool


class Income:
    def create(self, income, userID):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO income
                    (
                        name,
                        annualized_pay,
                        pay_frequency,
                        userID
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        income.name,
                        income.annualized_pay,
                        income.pay_frequency,
                        userID,
                    ],
                )
                id = result.fetchone()[0]
                data = income.dict()
                return IncomeOut(id=id, **data)

    def getlist(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM income
                        WHERE userID = %s
                        ORDER BY annualized_pay
                        """,
                        [userID],
                    )
                    a = [
                        IncomeOut(
                            id=record[0],
                            name=record[1],
                            annualized_pay=record[2],
                            pay_frequency=record[3],
                        )
                        for record in result
                    ]
                    response = IncomeList(income_list=a)
                    return response
        except Exception as e:
            print(e)
            return {"message": "Could not get all income"}

    def getindv(self, id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM income
                        WHERE id = %s AND userID = %s
                        """,
                        [id, userID],
                    )
                    record = result.fetchone()
                    if record is None:
                        return {"message": "The income id is not income"}
                    return self.out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get income"}

    def delete(self, income_id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM income
                        WHERE id = %s AND userID = %s
                        """,
                        [income_id, userID],
                    )
                    return True
        except Exception:
            return False

    def edit(self, id, income, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE income
                        SET name = %s, pay_frequency = %s, annualized_pay = %s
                        WHERE id = %s AND userID = %s
                        """,
                        [
                            income.name,
                            income.pay_frequency,
                            income.annualized_pay,
                            id,
                            userID,
                        ],
                    )
                    data = income.dict()
                    return IncomeOut(id=id, **data)
        except Exception as e:
            print(e)
            return {"message": "Could not update income"}

    def out(self, record):
        return IncomeOut(
            id=record[0],
            name=record[1],
            annualized_pay=record[2],
            pay_frequency=record[3],
        )

    def total(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT SUM(annualized_pay) as total_income
                        FROM income
                        WHERE userID = %s
                        """,
                        [userID],
                    )
                    total = result.fetchone()[0]
                    return total
        except Exception as e:
            print(e)
            return {"message": "Could not get the total income annualized_pay"}
