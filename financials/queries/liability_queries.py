from models.liabilities_models import LiabilityOut, LiabilitiesList
from queries.pool import pool


class Liabilities:
    def create(self, liability, userID):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO liabilities
                    (
                        name,
                        type,
                        value,
                        userID
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [liability.name, liability.type, liability.value, userID],
                )
                id = result.fetchone()[0]
                data = liability.dict()
                return LiabilityOut(id=id, **data)

    def getlist(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM liabilities
                        WHERE userID = %s
                        ORDER BY value
                        """,
                        [userID],
                    )
                    a = [self.out(record) for record in result]
                    response = LiabilitiesList(liabilities=a)
                    return response
        except Exception:
            return {"message": "Could not get all customers"}

    def getindv(self, id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        name,
                        type,
                        value
                        FROM liabilities
                        WHERE id = %s AND userID=%s
                        """,
                        [id, userID],
                    )
                    record = result.fetchone()
                    if record is None:
                        return {"message": "The liability id is not liability"}
                    return self.out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get liability"}

    def delete(self, liability_id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM liabilities
                        WHERE id = %s AND userID = %s
                        """,
                        [liability_id, userID],
                    )
                    return True
        except Exception:
            return False

    def edit(self, id, liability, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE liabilities
                        SET name = %s, type = %s, value = %s
                        WHERE id = %s AND userID = %s
                        """,
                        [
                            liability.name,
                            liability.type,
                            liability.value,
                            id,
                            userID,
                        ],
                    )
                    data = liability.dict()
                    return LiabilityOut(id=id, **data)
        except Exception as e:
            print(e)
            return {"message": "Could not update liability"}

    def out(self, record):
        return LiabilityOut(
            id=record[0], name=record[1], value=record[3], type=record[2]
        )

    def total(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT SUM(value) as total_liabilities
                        FROM liabilities
                        WHERE userID = %s
                        """,
                        [userID],
                    )
                    result = result.fetchone()[0]
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get the total liability value"}
