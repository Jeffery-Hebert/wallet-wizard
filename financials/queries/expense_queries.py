from models.expenses import (
    ExpenseOut,
    ExpensesList,
    ExpenseTypeOut,
    ExpensesTypeList,
)
from queries.pool import pool


class Expenses:
    def create(self, expense, userID):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO expenses
                    (
                        name,
                        amount,
                        type,
                        userID
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [expense.name, expense.amount, expense.type, userID],
                )
                id = result.fetchone()[0]
                data = expense.dict()
                return ExpenseOut(id=id, **data)

    def getlist(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM expenses
                        WHERE userID = %s
                        ORDER BY amount
                        """,
                        [userID],
                    )
                    a = [
                        ExpenseOut(
                            id=record[0],
                            name=record[1],
                            amount=record[2],
                            type=record[3],
                        )
                        for record in result
                    ]
                    response = ExpensesList(expenses=a)
                    return response
        except Exception as e:
            print(e)
            return {"message": "Could not get all expenses"}

    def getindv(self, id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM expenses
                        WHERE id = %s AND userID = %s
                        """,
                        [id, userID],
                    )
                    record = result.fetchone()
                    if record is None:
                        return {"message": "The expense id is not expense"}
                    return self.out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get expense"}

    def delete(self, expense_id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM expenses
                        WHERE id = %s AND userID = %s
                        """,
                        [expense_id, userID],
                    )
                    return True
        except Exception:
            return False

    def edit(self, id, expense, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE expenses
                        SET name = %s, type = %s, amount = %s
                        WHERE id = %s AND userID = %s
                        """,
                        [
                            expense.name,
                            expense.type,
                            expense.amount,
                            id,
                            userID,
                        ],
                    )
                    data = expense.dict()
                    return ExpenseOut(id=id, **data)
        except Exception as e:
            print(e)
            return {"message": "Could not update expense"}

    def out(self, record):
        return ExpenseOut(
            id=record[0], name=record[1], amount=record[2], type=record[3]
        )

    def total(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT SUM(amount) as total_expenses
                        FROM expenses
                        WHERE userID = %s
                        """,
                        [userID],
                    )
                    result = result.fetchone()[0]
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get the total expense amount"}

    def totalByCategory(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT type, SUM(amount) AS total_amount
                        FROM expenses
                        WHERE userID = %s
                        GROUP BY type
                        """,
                        [userID],
                    )
                    a = [
                        ExpenseTypeOut(type=record[0], total=record[1])
                        for record in result
                    ]
                    response = ExpensesTypeList(types=a)
                    return response
        except Exception as e:
            print(e)
            return {
                "message": "Could not get the total expense by category amount"
            }
