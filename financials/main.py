from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import assets, liabilities, net_worth, expenses, income, advisor
import uvicorn

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(assets.router)
app.include_router(liabilities.router)
app.include_router(net_worth.router)
app.include_router(expenses.router)
app.include_router(income.router)
app.include_router(advisor.router)


# Enable keep-alive only in the production environment
# Created an environment variable "APP_ENV" and set it to "production"

if os.environ.get("APP_ENV") == "production":
    print("keep alive")
    keep_alive = True
    keep_alive_timeout = 5
else:
    keep_alive = False
    keep_alive_timeout = None

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host="0.0.0.0",
        port=80,
        keep_alive=keep_alive,
        keep_alive_timeout=keep_alive_timeout,
    )
