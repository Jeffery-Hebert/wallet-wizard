// api_url.js
const api_url = `${process.env.REACT_APP_FINANCIALS_API_HOST}/api/`;

export default api_url;
