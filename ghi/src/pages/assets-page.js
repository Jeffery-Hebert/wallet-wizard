import { AssetList } from "../components/list-assets"
import CreateAssetFormWithAuth from "../components/create-assets-form"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const AssetsPage = () => (
  <PageLayout>
    <AssetList />
    <CreateAssetFormWithAuth />
  </PageLayout>
)
