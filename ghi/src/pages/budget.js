import { Budget } from "../components/Budget";
import { PageLayout } from "../components/page-layout";
import React from "react";

export const BudgetPage = () => (
  <PageLayout>
    <Budget />
  </PageLayout>
);
