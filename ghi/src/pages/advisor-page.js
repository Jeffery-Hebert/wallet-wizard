import AdvisorChat from "../components/advisor-chat";
import { PageLayout } from "../components/page-layout";
import React from "react";

export const AdvisorPage = () => (
  <PageLayout>
    <AdvisorChat />
  </PageLayout>
);
