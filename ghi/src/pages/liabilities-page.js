import CreateLiabilityFormWithAuth from "../components/create-liabilities-form"
import { LiabilityList } from "../components/list-liabilities"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const LiabilitiesPage = () => (
  <PageLayout>
    <LiabilityList />
    <CreateLiabilityFormWithAuth />
  </PageLayout>
)
