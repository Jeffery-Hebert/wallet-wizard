import { Auth0Features } from "../components/auth0-features"
import { HeroBanner } from "../components/hero-banner"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const HomePage = () => (
  <PageLayout>
    <HeroBanner />
    <Auth0Features />
  </PageLayout>
)
