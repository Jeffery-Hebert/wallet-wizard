import EditAssetFormWithAuth from "../components/edit-assets-form"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const EditAssetsPage = () => (
  <PageLayout>
    <EditAssetFormWithAuth />
  </PageLayout>
)
