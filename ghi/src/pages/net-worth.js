import { NetWorth } from "../components/net-worth"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const NetWorthPage = () => (
  <PageLayout>
    <NetWorth />
  </PageLayout>
)
