import { useEffect, useState } from "react"

import api_url from "../api_url"
import { useAuth0 } from "@auth0/auth0-react"
import { useNavigate } from "react-router-dom"

export const ExpenseList = () => {
  const [expenses, setExpenses] = useState([])
  const [error, setError] = useState(false)
  const navigate = useNavigate()
  const { getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    async function fetchExpenses() {
      try {
        const accessToken = await getAccessTokenSilently()
        const expensesUrl = `${api_url}expenses`
        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
        const expensesResponse = await fetch(expensesUrl, fetchConfig)
        let expensesData = await expensesResponse.json()
        setExpenses(expensesData.expenses)
      } catch (e) {
        console.error(e)
        setError(true)
      }
    }
    fetchExpenses()
  }, [getAccessTokenSilently])

  async function deleteExpenses(id) {
    try {
      const accessToken = await getAccessTokenSilently()
      const deleteUrl = `${api_url}expense/delete/${id}`
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      }

      const delResponse = await fetch(deleteUrl, fetchConfig)
      if (delResponse.ok) {
        window.location.reload()
      } else {
        console.error("Error deleting expense")
      }
    } catch (e) {
      console.error(e)
    }
  }

  if (error) {
    return <h1>Error fetching expenses</h1>
  }
  if (expenses.length > 0) {
    return (
      <>
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-12">
              <h1 className="mb-4 text-center">Expenses</h1>
              <table className="table table-striped table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th>Expense Name</th>
                    <th>Amount</th>
                    <th>Expense Category</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {expenses.map((expenses) => {
                    return (
                      <tr key={expenses.id}>
                        <td>{expenses.name}</td>
                        <td>${expenses.amount.toLocaleString()}</td>
                        <td>{expenses.type}</td>
                        <td>
                          <button
                            className="btn btn-secondary"
                            onClick={() => navigate(`${expenses.id}`)}
                          >
                            {" "}
                            Edit{" "}
                          </button>
                        </td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => deleteExpenses(expenses.id)}
                          >
                            {" "}
                            X
                          </button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    )
  }
}
