import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";

import api_url from "../api_url";
import { useState } from "react";

const pay_frequencies = [
  { label: "Weekly", value: "Weekly" },
  { label: "Bi-Weekly", value: "Bi-Weekly" },
  { label: "Twice a month", value: "Twice a month" },
  { label: "Monthly", value: "Monthly" },
  { label: "Other", value: "Other" },
];

const CreateIncomeForm = () => {
  const [name, setName] = useState("");
  const [annualized_pay, setAnnualizedPay] = useState("");
  const [pay_frequency, setPayFrequency] = useState("Weekly");

  const { getAccessTokenSilently } = useAuth0();

  async function createIncome(data) {
    const url = `${api_url}income/new`;

    const accessToken = await getAccessTokenSilently();

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    };

    try {
      await fetch(url, fetchConfig);

      window.location.reload();
    } catch (error) {
      console.error("Failed to create asset:", error);
    }
  }

  function handleSubmit(e) {
    e.preventDefault();

    createIncome({
      name: name,
      annualized_pay: annualized_pay,
      pay_frequency: pay_frequency,
    });
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Create a new Income!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={annualized_pay}
              onChange={(e) => setAnnualizedPay(e.target.value)}
              className="form-control"
              id="AnnualizedPay"
              rows="3"
              placeholder="AnnualizedPay"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">
              When do you need to have the full balance paid off by?
            </label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setPayFrequency(e.target.value)}
              required
            >
              <option value="" disabled>
                Select a Pay Frequency
              </option>
              {pay_frequencies.map((frequency) => (
                <option key={frequency.value} value={frequency.value}>
                  {frequency.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  );
};

const CreateIncomeFormWithAuth = withAuthenticationRequired(CreateIncomeForm);

export default CreateIncomeFormWithAuth;
