import { useEffect, useState } from "react";

import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";

export const AssetList = () => {
  const [assets, setAssets] = useState([]);
  const [error, setError] = useState(false);
  const navigate = useNavigate();
  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    const fetchAsset = async () => {
      try {
        const accessToken = await getAccessTokenSilently();
        const assetUrl = `${api_url}assets`;
        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        };
        const assetResponse = await fetch(assetUrl, fetchConfig);
        const assetData = await assetResponse.json();
        setAssets(assetData.assets);
      } catch (e) {
        console.error(e);
        setError(true);
      }
    };
    fetchAsset();
  }, [getAccessTokenSilently]);

  async function deleteAsset(id) {
    try {
      const accessToken = await getAccessTokenSilently();
      const deleteUrl = `${api_url}asset/delete/${id}`;
      const fetchConfig = {
        method: "delete",
        headers: {
          "content-type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      };

      const delResponse = await fetch(deleteUrl, fetchConfig);
      if (delResponse.ok) {
        window.location.reload();
      } else {
        console.error("Error deleting asset");
      }
    } catch (e) {
      console.error(e);
    }
  }

  if (error) {
    return <h1>Error fetching assets</h1>;
  }
  if (assets.length > 0) {
    return (
      <>
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-12">
              <h1 className="mb-4 text-center">Assets</h1>
              <table className="table table-striped table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th>Asset Name</th>
                    <th>Asset Value</th>
                    <th>Asset Type</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {assets.map((asset) => {
                    return (
                      <tr key={asset.id}>
                        <td>{asset.name}</td>
                        <td>${asset.value.toLocaleString()}</td>
                        <td>{asset.type}</td>
                        <td>
                          <button
                            className="btn btn-secondary"
                            onClick={() => navigate(`${asset.id}`)}
                          >
                            {" "}
                            Edit{" "}
                          </button>
                        </td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => deleteAsset(asset.id)}
                          >
                            {" "}
                            X
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
};
