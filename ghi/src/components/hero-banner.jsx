import React from "react";
import logo from "./FZlogo.png";

export const HeroBanner = () => {
  return (
    <div className="hero-banner hero-banner--pink-yellow">
      <div className="hero-banner__logo">
        <img className="hero-banner__image" src={logo} alt="React logo" />
      </div>
      <h1 className="hero-banner__headline">Hello, Friends!</h1>
      <p className="hero-banner__description">
        This is our personal finance tracker. We hope you like it.
      </p>
      <p className="hero-banner__description">
        THIS APP IS FOR DEMO PURPOSES ONLY!
      </p>
      <p className="hero-banner__description">
        You are strongly encouraged to create your own account. However a Test
        account has been created for you with some dummy data.
      </p>
      <p className="hero-banner__description">
        Please note due to restrictions placed by capRover the database
        terminates its connection when not in use. To "wake it up" you will need
        to click between a few tabs. After that the app should work like normal.
      </p>
      <p className="hero-banner__description">
        {" "}
        Username:wallet.wizard.test@gmail.com pw: Test123!
      </p>
    </div>
  );
};
