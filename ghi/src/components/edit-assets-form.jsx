import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react"
import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"

import api_url from "../api_url"

const types = [
  { label: "Liquid", value: "liquid" },
  { label: "Semi-Liquid", value: "semi-liquid" },
  { label: "Illiquid", value: "illiquid" },
]

export const EditAssetForm = () => {
  const { id } = useParams()
  const [name, setName] = useState("")
  const [value, setValue] = useState("")
  const [type, setType] = useState("")
  const navigate = useNavigate()

  const { getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    async function fetchAsset() {
      try {
        const assetUrl = `${api_url}asset/${id}`
        const accessToken = await getAccessTokenSilently()
        const assetResponse = await fetch(assetUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        let assetData = await assetResponse.json()
        setType(assetData.type)
        setName(assetData.name)
        setValue(assetData.value)
      } catch (error) {
        console.error(error)
      }
    }
    fetchAsset()
  }, [getAccessTokenSilently, id])

  async function editAsset(data) {
    const url = `${api_url}asset/${id}`

    const accessToken = await getAccessTokenSilently()

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }
    await fetch(url, fetchConfig)
    navigate(`/assets`)
  }

  async function handleSubmit(e) {
    e.preventDefault()
    editAsset({
      name: name,
      value: value,
      type: type,
    })
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Edit an exsisting Asset!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            />
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">Choose a type:</label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option
                value=""
                disabled
              >
                Select a Type
              </option>
              {types.map((type) => (
                <option
                  key={type.value}
                  value={type.value}
                  defaultValue={type.value === type}
                >
                  {type.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Edit</button>
        </form>
      </div>
    </div>
  )
}

const EditAssetFormWithAuth = withAuthenticationRequired(EditAssetForm)

export default EditAssetFormWithAuth
