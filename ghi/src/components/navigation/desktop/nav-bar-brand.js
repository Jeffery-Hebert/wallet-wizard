import { NavLink } from "react-router-dom";
import React from "react";

import logo from "./Logo.png";

// TODO: Update Desktop Nav Bar

export const NavBarBrand = () => {
  return (
    <div className="nav-bar__brand">
      <NavLink to="/">
        <img className="nav-bar__logo" src={logo} alt="Wallet Wizard logo" />
      </NavLink>
    </div>
  );
};
