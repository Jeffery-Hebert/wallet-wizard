import { MobileNavBarTab } from "./mobile-nav-bar-tab";
import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const MobileNavBarTabs = ({ handleClick }) => {
  const { isAuthenticated } = useAuth0();

  return (
    <div className="mobile-nav-bar__tabs">
      <MobileNavBarTab path="/" label="Home" handleClick={handleClick} />
      {isAuthenticated && (
        <>
          <MobileNavBarTab
            path="/networth"
            label="Net Worth"
            handleClick={handleClick}
          />
          <MobileNavBarTab
            path="/assets"
            label="Assets"
            handleClick={handleClick}
          />
          <MobileNavBarTab
            path="/liabilities"
            label="Liabilities"
            handleClick={handleClick}
          />
          <MobileNavBarTab
            path="/income"
            label="Income"
            handleClick={handleClick}
          />
          <MobileNavBarTab
            path="/expenses"
            label="Expenses"
            handleClick={handleClick}
          />
          <MobileNavBarTab
            path="/advisor"
            label="Ask an AI Advisor"
            handleClick={handleClick}
          />
        </>
      )}
    </div>
  );
};
