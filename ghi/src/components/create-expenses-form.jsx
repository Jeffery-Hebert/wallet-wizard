import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react"

import api_url from "../api_url"
import { useState } from "react"

const types = [
  { label: "Car", value: "Car" },
  { label: "Housing", value: "Housing" },
  { label: "Utilities", value: "Utilities" },
  { label: "Food", value: "Food" },
  { label: "Phone/TV", value: "Phone/TV" },
  { label: "Entertainment/Fun", value: "Entertainment/Fun" },
  { label: "Other", value: "Other" },
]

const CreateExpensesForm = () => {
  const [name, setName] = useState("")
  const [amount, setAmount] = useState("")
  const [type, setType] = useState("Car")

  const { getAccessTokenSilently } = useAuth0()

  async function createExpense(data) {
    const url = `${api_url}expenses/new`

    const accessToken = await getAccessTokenSilently()

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }

    try {
      await fetch(url, fetchConfig)
      window.location.reload()
    } catch (error) {
      console.error("Failed to create expense:", error)
    }
  }

  async function handleSubmit(e) {
    e.preventDefault()

    createExpense({
      name: name,
      amount: amount,
      type: type,
    })
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Create a new Expense!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              className="form-control"
              id="Amount"
              rows="3"
              placeholder="Amount"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">
              What category would you put this expense in?
            </label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option
                value=""
                disabled
              >
                Select an Expense Category
              </option>
              {types.map((frequency) => (
                <option
                  key={frequency.value}
                  value={frequency.value}
                >
                  {frequency.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  )
}

const CreateExpensesFormWithAuth =
  withAuthenticationRequired(CreateExpensesForm)

export default CreateExpensesFormWithAuth
