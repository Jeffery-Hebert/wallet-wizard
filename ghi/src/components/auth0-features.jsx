import { Auth0Feature } from "./auth0-feature";
import React from "react";

export const Auth0Features = () => {
  const featuresList = [
    {
      title: "Net Worth",
      description:
        "Track your net worth over time with our powerful graphing tools. See how your assets, investments, and debts are contributing to your overall financial health.",
      icon: "https://cdn.auth0.com/blog/hello-auth0/identity-providers-logo.svg",
    },
    {
      title: "Assets & Liabilities",
      description:
        "View all of your assets and liabilities in one place. Easily manage your investments, property, and debts to get a comprehensive picture of your financial situation.",
      icon: "https://cdn.auth0.com/blog/hello-auth0/mfa-logo.svg",
    },
    {
      title: "Income & Expenses",
      description:
        "Track your income and expenses with ease. Our app makes it simple to see where your money is going and how you can make better financial decisions.",
      icon: "https://cdn.auth0.com/blog/hello-auth0/advanced-protection-logo.svg",
    },
    {
      title: "Security by Auth0",
      description:
        "Rest easy knowing your financial information is secure with Auth0. Our app uses state-of-the-art security measures to protect your data and keep it safe from hackers and cyber threats.",
      icon: "https://cdn.auth0.com/blog/hello-auth0/private-cloud-logo.svg",
    },
  ];

  return (
    <div className="auth0-features">
      <h2 className="auth0-features__title">Explore Auth0 Features</h2>
      <div className="auth0-features__grid">
        {featuresList.map((feature) => (
          <Auth0Feature
            key={feature.title}
            title={feature.title}
            description={feature.description}
            icon={feature.icon}
          />
        ))}
      </div>
    </div>
  );
};
