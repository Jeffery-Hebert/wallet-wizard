import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react"
import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"

import api_url from "../api_url"

const types = [
  { label: "90 days or less", value: "short-term" },
  { label: "90-365 days", value: "intermediate-term" },
  { label: "365 days +", value: "long-term" },
]

export const EditLiabilityForm = () => {
  const { id } = useParams()
  const [name, setName] = useState("")
  const [value, setValue] = useState("")
  const [type, setType] = useState("short-term")
  const navigate = useNavigate()

  const { getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    async function fetchLiability() {
      try {
        const liabilityUrl = `${api_url}liability/${id}`
        const accessToken = await getAccessTokenSilently()
        const liabilityResponse = await fetch(liabilityUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        let liabilityData = await liabilityResponse.json()
        setType(liabilityData.type)
        setName(liabilityData.name)
        setValue(liabilityData.value)
      } catch (error) {
        console.error(error)
      }
    }
    fetchLiability()
  }, [getAccessTokenSilently, id])

  async function editLiability(data) {
    const url = `${api_url}liability/${id}`

    const accessToken = await getAccessTokenSilently()

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }
    await fetch(url, fetchConfig)
    navigate(`/liabilities`)
  }

  function handleSubmit(e) {
    e.preventDefault()
    editLiability({
      name: name,
      value,
      type,
    })
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Edit an Exsisting Liability!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">
              When do you need to have the full balance paid off by?
            </label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option
                value=""
                disabled
              >
                Select a Type
              </option>
              {types.map((type) => (
                <option
                  key={type.value}
                  value={type.value}
                >
                  {type.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  )
}

const EditLiabilityFormWithAuth = withAuthenticationRequired(EditLiabilityForm)

export default EditLiabilityFormWithAuth
