import { useState, useEffect } from "react";
import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";

function AdvisorChat() {
  const [receivedResponse, setReceivedResponse] = useState(false);
  const [response, setResponse] = useState("");
  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    alert(
      "Do not act upon any advice given. Seek a human advisor. Due to cost restrictions you cannot ask follow up questions. Asking a new question will delete the old one. Keep questions short. Expect short-ish answers."
    );
  }, []); // empty dependency array to run only once on mount

  async function fetchResponse(query) {
    const accessToken = await getAccessTokenSilently();
    console.log("inside fetch function");
    let advisorUrl = `${api_url}advisor/new`;
    const fetchConfig = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
      body: JSON.stringify({ query }),
    };
    let temp = await fetch(advisorUrl, fetchConfig);
    let answer = await temp.json();
    setResponse(answer);
    setReceivedResponse(true);
  }
  const handleSubmit = (e) => {
    console.log("inside handle submit");
    e.preventDefault();
    fetchResponse(e.target.query.value);
  };
  return (
    <div className="container" style={{ maxWidth: "600px", marginTop: "50px" }}>
      <div
        style={{
          backgroundColor: "#f8f9fa",
          border: "1px solid #dee2e6",
          borderRadius: "5px",
          padding: "20px",
          minHeight: "400px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        {receivedResponse && (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              marginBottom: "10px",
            }}
          >
            <div
              style={{
                backgroundColor: "#0d6efd",
                borderRadius: "15px",
                padding: "10px",
                color: "black",
                maxWidth: "80%",
              }}
            >
              <p>{response}</p>
            </div>
          </div>
        )}
        <form onSubmit={handleSubmit} style={{ display: "flex" }}>
          <div
            className="form-group"
            style={{ flexGrow: "1", marginRight: "10px" }}
          >
            <input
              type="text"
              id="query"
              name="query"
              className="form-control"
              placeholder="Ask Advisor"
              style={{
                borderRadius: "20px",
                border: "1px solid #ced4da",
                padding: "5px 10px",
              }}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            style={{ borderRadius: "20px" }}
          >
            Send
          </button>
        </form>
      </div>
    </div>
  );
}

export default AdvisorChat;
