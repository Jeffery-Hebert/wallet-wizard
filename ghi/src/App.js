import "./styles/App.css";

import { Route, Routes } from "react-router-dom";

import { AssetsPage } from "./pages/assets-page";
import { AuthenticationGuard } from "./components/authentication-guard";
import { CallbackPage } from "./pages/callback-page";
import { EditAssetsPage } from "./pages/asset-edit-page";
import { EditExpensePage } from "./pages/expense-edit-page";
import { EditIncomePage } from "./pages/income-edit-page";
import { EditLiabilitiesPage } from "./pages/liability-edit-page";
import { ExpensesPage } from "./pages/expenses-page";
import { HomePage } from "./pages/home-page";
import { IncomePage } from "./pages/income-page";
import { LiabilitiesPage } from "./pages/liabilities-page";
import { NetWorthPage } from "./pages/net-worth";
import { BudgetPage } from "./pages/budget";
import { NotFoundPage } from "./pages/not-found-page";
import { PageLoader } from "./components/page-loader";
import { ProfilePage } from "./pages/profile-page";
import { useAuth0 } from "@auth0/auth0-react";
import { AdvisorPage } from "./pages/advisor-page";

export const App = () => {
  const { isLoading } = useAuth0();

  if (isLoading) {
    return (
      <div className="page-layout">
        <PageLoader />
      </div>
    );
  }

  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route
        path="networth"
        element={<AuthenticationGuard component={NetWorthPage} />}
      />
      <Route
        path="budget"
        element={<AuthenticationGuard component={BudgetPage} />}
      />
      <Route path="assets">
        <Route
          index
          path=""
          element={<AuthenticationGuard component={AssetsPage} />}
        />
        <Route
          path=":id"
          element={<AuthenticationGuard component={EditAssetsPage} />}
        />
      </Route>
      <Route path="liabilities">
        <Route
          index
          path=""
          element={<AuthenticationGuard component={LiabilitiesPage} />}
        />
        <Route
          path=":id"
          element={<AuthenticationGuard component={EditLiabilitiesPage} />}
        />
      </Route>
      <Route path="income">
        <Route
          index
          path=""
          element={<AuthenticationGuard component={IncomePage} />}
        />
        <Route
          path=":id"
          element={<AuthenticationGuard component={EditIncomePage} />}
        />
      </Route>
      <Route path="expenses">
        <Route
          index
          path=""
          element={<AuthenticationGuard component={ExpensesPage} />}
        />
        <Route
          path=":id"
          element={<AuthenticationGuard component={EditExpensePage} />}
        />
      </Route>
      <Route path="advisor">
        <Route
          index
          path=""
          element={<AuthenticationGuard component={AdvisorPage} />}
        />
      </Route>
      <Route
        path="profile"
        element={<AuthenticationGuard component={ProfilePage} />}
      />
      <Route path="callback" element={<CallbackPage />} />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};
