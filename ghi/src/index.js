import "./styles/styles.css"

import { App } from "./App"
import { Auth0ProviderWithNavigate } from "./auth0-provider-with-navigate"
import { BrowserRouter } from "react-router-dom"
import React from "react"
import { createRoot } from "react-dom/client"

const container = document.getElementById("root")
const root = createRoot(container)

root.render(
  <React.StrictMode>
    <BrowserRouter basename={process.env.REACT_APP_BASENAME}>
      <Auth0ProviderWithNavigate>
        <App />
      </Auth0ProviderWithNavigate>
    </BrowserRouter>
  </React.StrictMode>
)
